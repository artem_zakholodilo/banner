<?php

require_once __DIR__ . '/vendor/autoload.php';

use App\Services\Banner;
use App\Services\MysqlConnection;

const FILE_NAME = "1.jpg";

$banner = new Banner(new MysqlConnection());
try {
    $banner->setUserInfo();
} catch (\Exception $ex) {
    // todo log message
}

header('Content-Type: image/jpeg');
header('Content-Length: ' . filesize(FILE_NAME));
readfile(FILE_NAME);
