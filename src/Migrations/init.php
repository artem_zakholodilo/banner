<?php

require_once __DIR__ . '/../../vendor/autoload.php';

use App\Services\MysqlConnection;

$connection = (new MysqlConnection())->getConnection();

$createDatabase = "CREATE DATABASE banner";

$useDb = "USE banner";

$createTableCommand = "
    CREATE TABLE banner_stat(
    ip_address varchar(80) not null unique, user_agent varchar(80), view_date timestamp,
    page_url varchar(80) not null unique , views_count int(100) 
    );
";

try {
    $connection->exec($createDatabase);

    $connection->exec($useDb);

    $connection->exec($createTableCommand);
} catch (\Exception $ex) {
    echo $ex->getMessage();
}
