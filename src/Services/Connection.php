<?php

namespace App\Services;

interface Connection
{
    public function getConfig();

    public function getConnection();

    /**
     * @param string $ipAddress
     * @param string $pageUrl
     * @return mixed|bool
     */
    public function getUserInfo(string $ipAddress, string $pageUrl);

    /**
     * @param string $ipAddress
     * @param string $userAgent
     * @param string $pageUrl
     * @return bool
     */
    public function setUserInfo(string $ipAddress, string $userAgent, string $pageUrl): bool;

    /**
     * @param string $ipAddress
     * @param string $userAgent
     * @param string $pageUrl
     * @return bool
     */
    public function updateUserInfo(string $ipAddress, string $userAgent, string $pageUrl): bool;
}