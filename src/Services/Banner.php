<?php

namespace App\Services;

use App\Helpers\UserInfo;

class Banner
{
    /**
     * @var Connection|null $connection
     */
    protected $connection = null;

    /**
     * @var string $ipAddress user ip address
     */
    protected $ipAddress;

    /**
     * @var string $pageUrl URL that user visited
     */
    protected $pageUrl;

    /**
     * Banner constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        $this->ipAddress = UserInfo::getIp();
        $this->pageUrl = UserInfo::getUrlAddress();
    }

    /**
     * @return bool
     */
    public function setUserInfo()
    {
        if ($this->issetUserInfo()) {
            return $this->updateUserStatistic();
        } else {
            return $this->setUserVisited();
        }
    }

    /**
     * @return bool
     */
    protected function issetUserInfo(): bool
    {
        if ($this->connection->getUserInfo($this->ipAddress, $this->pageUrl)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    protected function updateUserStatistic(): bool
    {
        if ($this->connection->updateUserInfo($this->ipAddress, $this->pageUrl, UserInfo::getAgent())) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    protected function setUserVisited(): bool
    {
        if ($this->connection->setUserInfo($this->ipAddress, $this->pageUrl, UserInfo::getAgent())) {
            return true;
        }

        return false;
    }
}