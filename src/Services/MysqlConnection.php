<?php

namespace App\Services;

class MysqlConnection implements Connection
{
    /**
     * @var array $config
     */
    protected $config;

    /**
     * @var resource $connection
     */
    protected $connection;

    /**
     * @return \PDO|resource
     */
    public function getConnection()
    {
        $config = $this->getConfig()['mysql'];
        $dsn = "mysql:host={$config['host']}";
        $this->connection = new \PDO($dsn, $config['user'], $config['password']);
        if ($this->checkDatabase()) {
            $dbName = $config['dbname'];
            $this->connection->exec("USE {$dbName}");
        }
        return $this->connection;
    }

    /**
     * @return array|mixed
     */
    public function getConfig()
    {
        $this->config = require __DIR__ . "/../../config/config.php";

        return $this->config;
    }

    /**
     * @param string $ipAddress
     * @param string $pageUrl
     * @return bool
     */
    public function getUserInfo(string $ipAddress, string $pageUrl)
    {
        $sql = "SELECT COUNT(ip_address, page_url) FROM banner_stat 
                WHERE ip_address = {$ipAddress} AND page_url = {$pageUrl} LIMIT 1";
        $result = $this->connection->query($sql);
        if ($result) {
            return true;
        }

        return false;
    }

    /**
     * @todo update methods
     * @link https://dev.mysql.com/doc/refman/8.0/en/insert-on-duplicate.html
     * @var string $ipAddress
     * @var string $pageUrl
     * @var string $userAgent
     * @return bool
     */
    public function setUserInfo(string $ipAddress, string $pageUrl, string $userAgent): bool
    {

        $sql = "INSERT INTO banner_stat(ip_address, user_agent, view_date, page_url, views_count) 
               VALUES({$ipAddress}, {$userAgent}, now(), {$pageUrl}, 1)";
        if ($this->connection->exec($sql)) {
            return true;
        }

        return  false;
    }

    /**
     * @var string $ipAddress
     * @var string $pageUrl
     * @var string $userAgent
     * @return bool
     */
    public function updateUserInfo(string $ipAddress, string $pageUrl, string $userAgent): bool
    {
        $sql = "UPDATE banner_stat SET view_date = view_date + 1, user_agent = {$userAgent} 
                WHERE ip_address = {$ipAddress} AND page_url = {$pageUrl}";
        if ($this->connection->exec($sql)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function checkDatabase()
    {
        $dbName = $this->getConfig()['mysql']['dbname'];
        $stmt = $this->connection->query(
            "SELECT COUNT(*) 
                          FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '{$dbName}'"
        );
        return (bool) $stmt->fetchColumn();
    }
}