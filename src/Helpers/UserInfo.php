<?php

namespace App\Helpers;

class UserInfo
{
    /**
     * @var string $defaultUrl default url visited by user
     */
    private static $defaultUrl = 'index1.html';

    /**
     * @return string|mixed
     */
    public static function getIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    /**
     * @return string
     */
    public static function getUrlAddress()
    {
        return $_SERVER['HTTP_REFERER'] ?? static::$defaultUrl;
    }

    /**
     * @return mixed
     */
    public static function getAgent()
    {
        return $_SERVER['HTTP_USER_AGENT'];
    }
}